package objectsAndClasses;

public abstract class Trade {
    private String id;
    private String symbol;
    private int quantity;

    private double price;

    public Trade(String id, String symbol, int quantity, double price) {
        this.id = id;
        this.symbol = symbol;
        this.quantity = quantity;
        this.price = price;
    }

    public Trade(String id, String symbol, int quantity) {
        this(id, symbol, quantity, 0);
    }

    public String toString() {
        return ("ID :" + id + "\n" + "symbol : " + symbol + "\n"
                + "Quantity : " + quantity + "\n" + "Price : " + price);

    }

    public void setPrice(double price) {
        if (price >= 0)
            this.price = price;
    };

    public double getPrice() {
        return price;

    }

    public double getValueOfTrade() {
        return price * quantity;
    }

    abstract public double calcDividend();
}
