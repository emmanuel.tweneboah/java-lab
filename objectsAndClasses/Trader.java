package objectsAndClasses;

public class Trader {
    private String name;

    private Account account;

    public Trader(String name, Account account) {
        this.name = name;
        this.account = account;
    }

    public double addTrade(Trade trade) {
        return account.setTotalValue(trade.getValueOfTrade());
    }

}
