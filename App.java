import org.junit.jupiter.api.extension.TestWatcher;

import objectsAndClasses.Client;
import objectsAndClasses.MembershipType;
import objectsAndClasses.MembershipTypeEnum;
import objectsAndClasses.SilverMember;
import objectsAndClasses.Trade;

public class App {

    public static void main(String[] args) {
        System.out.println("Hello world!");

        // Trade trade1 = new Trade("T1", "APPL",100,15.25);
        // trade1.setPrice(12);

        // Trade trade2 = new Trade("T2", "AtL", 30);
        // System.out.println(trade1.toString());
        // System.out.println("---------------------");
        // System.out.println(trade2.toString());
        Client client = new Client("Aikins", "ME");
        Client client1 = new Client("Hekk", "djfsd");
        System.out.println(client1.getStatus());
        client.addTrade();
        client.addTrade();
        System.out.println(client.getPoints());
        client.addTrade();
        client.addTrade();
        client.addTrade();
        client.addTrade();
        client.addTrade();
        client.addTrade();
        client.addTrade();
        client.addTrade();
        client.addTrade();
        System.out.println(client.getPoints());
        System.out.println(client.getStatus());

    }
}
