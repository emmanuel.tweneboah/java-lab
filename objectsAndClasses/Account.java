package objectsAndClasses;

public class Account {
    private double totalValue;

    public double setTotalValue (double totalValue){
      return this.totalValue += totalValue;
    }

    public double getTotalAccountValue(){
        return this.totalValue;
    }

}
