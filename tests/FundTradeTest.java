package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import objectsAndClasses.FundTrade;

public class FundTradeTest {
    @Test
    void testCalcDividend() {
        double price =60;
        double percentage =0.5;
        double dividend = price*percentage;
     FundTrade fund = new FundTrade("Tuy", "YFH", 5, 60, 0.5);
     assertEquals(fund.calcDividend(), dividend);

    }
}
