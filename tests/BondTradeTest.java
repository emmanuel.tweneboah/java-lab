package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import objectsAndClasses.BondTrade;

public class BondTradeTest {
    @Test
    void testCalcDividend() {
        double dividend = 105;
        BondTrade bond = new BondTrade("Tuy", "YFH", 5, 60, 105);
        assertEquals(bond.calcDividend(), dividend);
    }
}
