package objectsAndClasses;

public enum MembershipTypeEnum {
    BRONZE, GOLD, SILVER;

    MembershipTypeEnum progress (int points) {
        if (points < 10) return BRONZE;
        else if (points > 10 && points < 19) return SILVER;
        else if (points > 19 ) return GOLD;
        
    }

}
