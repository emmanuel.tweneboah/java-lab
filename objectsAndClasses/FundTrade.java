package objectsAndClasses;
public class FundTrade extends Trade {
    private double percentage; 

    public FundTrade(String id, String symbol, int quantity, double price, double percentage){
        super(id,symbol,quantity,price);
        this.percentage = percentage;
    }

    public double calcDividend(){
       return this.getPrice() * percentage;
    }
    
}
