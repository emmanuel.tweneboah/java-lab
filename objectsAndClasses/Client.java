package objectsAndClasses;

public class Client {
    private String firstName;
    private String lastName;
    private MembershipTypeEnum status = null;
    private int points;

    public int getPoints() {
        return points;
    }

    public Client(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public MembershipTypeEnum getStatus() {
        return status;
    }

    public void addTrade() {
        points++;
        if (points >= 10) {
            status = MembershipTypeEnum.SILVER;
        } else if (points >= 20) {
            status = MembershipTypeEnum.GOLD;
        } else
            status = MembershipTypeEnum.BRONZE;
    }
}
